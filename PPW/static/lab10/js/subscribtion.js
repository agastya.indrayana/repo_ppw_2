$(document).ready(function() {
    $("#submitButton").attr("disabled", true);
    $("#emailInput").change(function() {
        var email = $(this).val();
        $.ajax({
            url: "/api/checkemail/",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                if(data.already_exist) {
                    alert("This email is already taken");
                    $("#submitButton").attr("disabled", true);
                } else {
                    $("#submitButton").attr("disabled", false);
                }
            }
        });
    });

    $("#submitButton").click(function(){
        event.preventDefault();
        var name = $("#nameInput").val();
        var email = $("#emailInput").val();
        var pass = $("#passwordInput").val();
        if(name.length == 0 || pass == 0){
            alert("check your name and password\nName and Password can't be empty");
        }else{
            $.ajax({
                headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
                url: "/api/register/",
                data: {
                    'name' : name,
                    'email' : email,
                    'password': pass,
                },
                method: "POST",
                dataType: "json",
                success: function(data){
                    alert("Hello " + data.name + " thank you for subscribing\nyou'll now be redirected to homepage")
                    window.location = "/"
                }
            })
        }
    });

});
