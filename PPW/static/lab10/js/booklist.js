var clicks = 0;
$(document).ready(function() {
    $("#searchButton").click(function() {
        var name = $("#bookTitle").val();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url: "/api/booklist/",
            data: {
                'name': name,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                $("#booksList").empty()
                clicks = 0;
                data.books.forEach(element => {
                    var txt = "<div class = 'well'><ul><h4>"+element.title +"</h4><br><small>By " + element.authors + "</small><br><p>"+ element.description + "</p>" + createButton(element.title) + "</ul></div>";
                    // txt.concat()
                    $("#booksList").append(txt);
                });
            }
        });
    });
});

$(document).on('click', ".addToFavorite", function() {
    var $this = $(this);
    $this.text("Remove from favorite");
    $this.removeClass("addToFavorite");
    $this.addClass("unFavorite");
    var name = $(this)[0].value;
    clicks += 1;
    document.getElementById("clicks").innerHTML = clicks;
})

$(document).on('click', ".unFavorite", function() {
    var $this = $(this);
    $this.text("Add to favorite");
    $this.addClass("addToFavorite");
    $this.removeClass("unFavorite");
    var name = $(this)[0].value;
    clicks -= 1;
    document.getElementById("clicks").innerHTML = clicks;
})


function createButton(name){
    var button = '<button class="addToFavorite btn btn-secondary" type="button" value="' + name +'">Add To Favorite</button>'
    return button
}
