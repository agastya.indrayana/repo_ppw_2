$(document).ready(function() {
    $("#refreshButton").click(function(){
        // $("#myTable").innerHTML = "";
        $.ajax({
            url: "/api/subscriber-list/",
            method: "GET",
            success: function(data) {
                $("#cards-content").empty();
                subscriberList = data.subscriberList
                subscriberList.forEach(element => {
                    var txt = "<div class = 'well'><ul>"+element.name +"<br><article>" + element.email + "</article>"+ createButton(element.email) + "</ul></div>";
                    $("#cards-content").append(txt);
                });
                $(".unsubscribeButton").click(function(){
                    var email = $(this)[0].value;
                    var password = window.prompt("Please enter your password", "your password");
                    if (password ==null || password ==""){
                        alert("action cancelled")
                    }else{
                        $.ajax({
                            url:"/api/delete-Validation/",
                            data:{
                                "email":email,
                                "password": password
                            },
                            method:"post",
                            dataType: 'json',
                            success: function(data){
                                if(data.success){
                                    alert("account " + data.name + " has unsubscribe")
                                    $("#refreshButton").click();
                                }else{
                                    alert("password doesn't match, please try again")
                                }
                            }
                        })
                    }
                });
                // alert(subscriberList)
                // alert(data.subscriberList)
                // console.log(data)
                // subscriberList = data.subscriberList
                // subscriberList.forEach(element => {
                //     
                // });
            }
        });
    });

});

function createButton(email){
    var button = '<button class="unsubscribeButton btn btn-primary" type="button" class="btn btn-secondary" value="' + email +'">Unsubscribe</button>'
    return button
}