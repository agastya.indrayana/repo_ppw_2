from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name="Index"),
    path('profile/', views.profile, name="Profile"),
    path('books/', views.books, name="books"),
    path('subscribe/', views.subscribe, name="subscribe"),
    path('subscriber-list/', views.subscriberList, name="subscribe"),
    path('api/subscriber-list/', views.subscriberListApi, name="subscriber list"),
    path('api/booklist/', views.booklist, name="booklist"),
    path('api/register/', views.register, name="register"),
    path('api/checkemail/', views.checkEmail, name="checkemail"),
    path('api/delete-Validation/', views.deleteValidation, name="delete-Validation"),
]
