from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
from .models import Users 



def index(request):
    return render(request, "lab10/index.html")

def profile(request):
    return render(request, "lab10/profile.html")

def books(request):
    return render(request, "lab10/books.html")

def booklist(request):
    if request.method == "POST":
        url = "https://www.googleapis.com/books/v1/volumes?q=" + request.POST.get("name")
        r = requests.get(url)
        data = r.json()
        rData = data["items"]
        books = []
        # print(rData)
        j=0
        for i in rData:
            if "description" in i["volumeInfo"]:
                description = i["volumeInfo"]["description"]
            elif "subtitle" in i["volumeInfo"]:
                description = i["volumeInfo"]["subtitle"]
            else:
                 description = "Sorry no description is available about this book"

            if "authors" in i["volumeInfo"]:
                authors = ''.join(i["volumeInfo"]["authors"])
            else:
                authors = "Sorry no author information is available about this book"

            k = {
                "title":i["volumeInfo"]["title"],
                "description":description,
                "authors":authors
            } 
            books.append(k)
        data = {"books":books}
        return JsonResponse(data)
    else:
        return JsonResponse(data={})

def subscribe(request):
    return render(request, "lab10/subscribe.html")

def register(request):
    data = {}
    if (request.method == "POST"):
        name = request.POST.get("name")
        email = request.POST.get("email")
        password = request.POST.get("password")
        newSubscriber = Users(name = name, email = email, password = password)
        newSubscriber.save()
        data = {"name":name}
    return JsonResponse(data)

@csrf_exempt
def checkEmail(request):
    email = request.POST.get("email")
    data = {"already_exist":Users.objects.filter(email=email).exists()}
    return JsonResponse(data)

def subscriberList(request):
    return render(request, "lab10/subscriberList.html")

def subscriberListApi(request):
    subscriberList = list(Users.objects.values())
    data = {"subscriberList" : subscriberList}
    return JsonResponse(data)

@csrf_exempt
def deleteValidation(request): 
    data = {}
    if (request.method == "POST"):
        email = request.POST.get("email")
        password = request.POST.get("password")
        user = list(Users.objects.filter(email=email).values())
        if len(user)==1:
            name = user[0]["name"]
            if(user[0]["password"] == password):
                Users.objects.filter(email=email).delete()
                data = {"success":True, "name":name}
            else:
                data = {"success":False, "name":name}
    return JsonResponse(data)
