from django.test import TestCase, Client
from django.urls import resolve
from . import views, models
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab10Test(TestCase):
    def test_lab_10_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'lab10/index.html')

    def test_lab_10_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_lab_10_subscribe_url_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_subscribe_using_index_template(self):
        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response, 'lab10/subscribe.html')

    def test_lab_10_subscribe_using_index_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, views.subscribe)

    def test_lab_10_api_validation_url_exist(self):
        response = Client().get('/api/checkemail/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_api_validation_using_index_func(self):
        found = resolve('/api/checkemail/')
        self.assertEqual(found.func, views.checkEmail)

    def test_lab_10_api_registration_url_exist(self):
        response = Client().get('/api/register/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_api_registration_using_index_func(self):
        found = resolve('/api/register/')
        self.assertEqual(found.func, views.register)
    
    def test_lab_10_api_registration_can_save_to_database(self):
        self.client.post('/api/register/', data={"name":"alice","email":"alice@company.com","password":"123456"})
        totalSubscribers = models.Users.objects.all().count()
        self.assertEqual(totalSubscribers, 1)
    
    def test_lab_10_subscriber_list_url_exist(self):
        response = Client().get('/subscriber-list/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_subscriber_list_using_index_func(self):
        found = resolve('/subscriber-list/')
        self.assertEqual(found.func, views.subscriberList)

    def test_lab_10_subscriber_list_using_index_template(self):
        response = Client().get('/subscriber-list/')
        self.assertTemplateUsed(response, 'lab10/subscriberList.html')

    def test_lab_10_subscriber_list_api_url_exist(self):
        response = Client().get('/api/subscriber-list/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_subscriber_list_api_using_function(self):
        found = resolve('/api/subscriber-list/')
        self.assertEqual(found.func, views.subscriberListApi)

    # dari mana dapet CSRF token nya???
    # def test_lab_10_subscriber_list_api_return_data(self):
    #     self.client.post('/api/subscriber-list/', data={"name":"alice","email":"alice@company.com","password":"123456"})
    #     totalSubscribers = models.Users.objects.all().count()
    #     self.assertEqual(totalSubscribers, 1)

    def test_lab_10_delete_validation_url_exist(self):
        response = Client().get('/api/delete-Validation/')
        self.assertEqual(response.status_code,200)

    def test_lab_10_delete_validation_using_function(self):
        found = resolve('/api/delete-Validation/')
        self.assertEqual(found.func, views.deleteValidation)

    def test_lab_10_api_delete_validation_works(self):
        #test email exist and password match
        self.client.post('/api/register/', data={"name":"alice","email":"alice@company.com","password":"123456"})
        response = self.client.post("/api/delete-Validation/", data={"email":"alice@company.com","password":"123456"})
        self.assertEqual(response.json()["success"], True)
        #test email exist, but password missmatch
        self.client.post('/api/register/', data={"name":"alice","email":"alice@company.com","password":"123456"})
        response = self.client.post("/api/delete-Validation/", data={"email":"alice@company.com","password":"123455"})
        self.assertEqual(response.json()["success"], False)
        #test email doesn't exist
        self.client.post('/api/register/', data={"name":"alice","email":"alice@company.com","password":"123456"})
        response = self.client.post("/api/delete-Validation/", data={"email":"bob@company.com","password":"123456"})
        self.assertEqual(response.json(), {})

    
    def test_lab_9_api_booklist_url_exist(self):
        response = Client().get('/api/booklist/')
        self.assertEqual(response.status_code,200)
    
    def test_lab_9_books_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_lab9_api_books_return_data(self):
        response = self.client.post("/api/booklist/", data = {"name":"Quilting"})
        self.assertGreater(len(response.json()["books"]),0)
    
    def test_lab_8_profile_url_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

# class lab10FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(lab10FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(lab10FunctionalTest, self).tearDown()
    
#     def test_form_validation_works(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/subscribe/')

#         submitButton = selenium.find_element_by_id('submitButton')
#         self.assertIn(submitButton.get_attribute("disabled"),"true")

#         emailField = selenium.find_element_by_id('emailInput')
#         nameField = selenium.find_element_by_id('nameInput')
#         passwordField = selenium.find_element_by_id('passwordInput')

#         emailField.send_keys("hobit@company.com")
#         nameField.send_keys("hobit")
#         passwordField.send_keys("123456")

#         self.assertIsNone(submitButton.get_attribute("disabled"))
